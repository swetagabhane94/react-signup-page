import React from 'react';
import './App.css';
import { Component } from 'react';
import Form from "./component/Form"


class App extends Component{


  render(){
    return (
      <div className="container p-2 mt-5 border border-primary">
      <h1 className="text-center text-primary">SignUp</h1>
      <hr/>
        <Form/>
        </div>
    )
  }
}

export default App;

