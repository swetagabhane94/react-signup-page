import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import validator from "validator";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
      role: "",
      email: "",
      password: "",
      repeatPassword: "",
      termsCondition: false,
      errorCase: {
        firstName: "",
        lastName: "",
        age: "",
        gender: "",
        role: "",
        email: "",
        password: "",
        repeatPassword: "",
        termsCondition: "",
      },
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  handleCheckboxChange = (event) => {
    this.setState({
      termsCondition: event.target.checked,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    let errors = {};
    if (!validator.isAlpha(this.state.firstName)) {
      errors.firstName = "First name must be letters";
    }

    if (!validator.isAlpha(this.state.lastName)) {
      errors.lastName = "Last name must be letters";
    }

    if (!validator.isInt(this.state.age) ) {
      errors.age = "Age must be a integer ";
    } 
    else if (( this.state.age < 0)){
      errors.age = "Age must be a positive number";
    } 
    else if (!(this.state.age <= 150 && this.state.age >= 1)) {
      errors.age = "Age must be a number between 1 and 150";
    }

    if (!this.state.gender) {
      errors.gender = "Gender is required";
    }

    if (!this.state.role) {
      errors.role = "Role is required";
    }

    if (!validator.isEmail(this.state.email)) {
      errors.email = "Email is not valid";
    }

    if (!validator.isStrongPassword(this.state.password)) {
      errors.password = (
        <div>
          Passwords must contain:
          <li> Minimum of 1 lower case letter</li>
          <li>Minimum of 1 upper case letter</li>
          <li>Minimum of 1 numeric character</li>
          <li> Minimum of 1 special character</li>
          <li> Passwords must be at least 8 characters in length</li>
        </div>
      );
    }

    if (this.state.password !== this.state.repeatPassword) {
      errors.repeatPassword = "Passwords do not match";
    }

    if (!this.state.termsCondition) {
      errors.termsCondition = "You must agree to the terms and conditions";
    }

    this.setState({ errorCase: errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ 
        hasValidated: true 
      });
    }
  };

  render() {
    const {
      firstName,
      lastName,
      age,
      gender,
      role,
      email,
      password,
      repeatPassword,
      termsCondition,
    } = this.state;

    return (
      <div>
        <form className={this.state.hasValidated && "d-none"}>
          <div className="row p-2 text-primary">
            <div className="col-md-4">
              <label htmlFor="firstName">
                <i className="fa-sharp fa-solid fa-user"></i>
                <strong> First Name </strong>
              </label>
              <input
                type="text"
                className="form-control"
                id="firstName"
                name="firstName"
                value={firstName}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      firstName: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
                placeholder="Enter Your First Name"
              />
              <small className="text-danger">
                {this.state.errorCase.firstName}
              </small>
            </div>

            <div className="col-md-4">
              <label htmlFor="lastName">
                <i className="fa-sharp fa-solid fa-user"></i>
                <strong> Last Name </strong>
              </label>
              <input
                type="text"
                className="form-control"
                id="lastName"
                name="lastName"
                value={lastName}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      lastName: "",
                    },
                  });

                  this.handleInputChange(event);
                }}
                placeholder="Enter Your Last Name"
              />
              <small className="text-danger">
                {this.state.errorCase.lastName}
              </small>
            </div>

            <div className="col-md-4">
              <label htmlFor="age">
                <b>Age</b>
              </label>
              <input
                type="number"
                className="form-control"
                id="age"
                name="age"
                value={age}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      age: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
                placeholder="Enter Your Present Age"
              />
              <small className="text-danger">{this.state.errorCase.age}</small>
            </div>
          </div>

          <div>
            <div className="col text-primary">
              <legend
                className="col-form-label col-sm-2 px-2"
                name="gender"
                value={gender}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      gender: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
              >
                <i class="fa-sharp fa-solid fa-venus-mars"></i>
                <strong> Gender</strong>
              </legend>
              <div className="form-check form-check-inline py-2">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio1"
                  value="option1"
                  checked={this.state.gender === "option1"}
                  onChange={(event) => {
                    this.setState({
                      gender: event.target.value,
                      errorCase: {
                        ...this.state.errorCase,
                        gender: "",
                      },
                    });

                    this.handleInputChange(event);
                  }}
                />
                <label className="form-check-label" htmlFor="inlineRadio1">
                  Male
                </label>
              </div>

              <div className="form-check form-check-inline py-2">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio2"
                  value="option2"
                  checked={this.state.gender === "option2"}
                  onChange={(event) => {
                    this.setState({
                      gender: event.target.value,
                      errorCase: {
                        ...this.state.errorCase,
                        gender: "",
                      },
                    });

                    this.handleInputChange(event);
                  }}
                />
                <label className="form-check-label" htmlFor="inlineRadio2">
                  Female
                </label>
              </div>

              <div className="form-check form-check-inline py-2">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio3"
                  value="option3"
                  checked={this.state.gender === "option3"}
                  onChange={(event) => {
                    this.setState({
                      gender: event.target.value,
                      errorCase: {
                        ...this.state.errorCase,
                        gender: "",
                      },
                    });

                    this.handleInputChange(event);
                  }}
                />
                <label className="form-check-label" htmlFor="inlineRadio3">
                  Other
                </label>
              </div>
              <small className="text-danger">
                {this.state.errorCase.gender}
              </small>
            </div>
          </div>

          <div className="col text-primary px-2">
            <label htmlFor="roleID">
              <i class="fa-sharp fa-solid fa-briefcase"></i>
              <strong> Role</strong>
            </label>
            <select
              id="roleID"
              className="form-select text-primary"
              value={role}
              onChange={(event) => {
                this.setState({
                  role: event.target.value,
                  errorCase: {
                    ...this.state.errorCase,
                    role: "",
                  },
                });
                this.handleInputChange(event);
              }}
            >
              <option value="">Select Your Role</option>
              <option value="1">Developer</option>
              <option value="2">Senior Developer</option>
              <option value="3">Lead Engineer</option>
              <option value="4">CTO</option>
            </select>
            <small className="text-danger">{this.state.errorCase.role}</small>
          </div>

          <div className="row p-2 text-primary">
            <div className="col-md-4 ">
              <label htmlFor="email">
                <i className="fa-sharp fa-solid fa-envelope text-primary"></i>
                <strong> Email </strong>
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                value={email}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      email: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
                placeholder="Enter Email"
              />
              <small className="text-danger">
                {this.state.errorCase.email}
              </small>
            </div>

            <div className="col-md-4">
              <label htmlFor="password">
                <i className="fa-sharp fa-solid fa-lock"></i>
                <strong> Password </strong>
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                value={password}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      password: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
                placeholder="Enter Password"
              />
              <small className="text-danger">
                {this.state.errorCase.password}
              </small>
            </div>

            <div className="col-md-4">
              <label htmlFor="repeatPassword">
                <i className="fa-sharp fa-solid fa-lock"></i>
                <strong> Repeat Password</strong>
              </label>
              <input
                type="password"
                className="form-control"
                id="repeatPassword"
                name="repeatPassword"
                value={repeatPassword}
                onChange={(event) => {
                  this.setState({
                    errorCase: {
                      ...this.state.errorCase,
                      repeatPassword: "",
                    },
                  });
                  this.handleInputChange(event);
                }}
                placeholder="Enter Repeat Password"
              />
              <small className="text-danger">
                {this.state.errorCase.repeatPassword}
              </small>
            </div>
          </div>

          <div className="form-check text-primary">
            <input
              type="checkbox"
              className="form-check-input"
              id="exampleCheck1"
              checked={termsCondition}
              onChange={(event) => {
                this.setState({
                  errorCase: {
                    ...this.state.errorCase,
                    termsCondition: "",
                  },
                });
                this.handleCheckboxChange(event);
              }}
            />
            <label className="form-check-label" htmlFor="exampleCheck1">
              I agree to the Terms and Conditions
            </label>
            <div>
              <small className="text-danger">
                {this.state.errorCase.termsCondition}
              </small>
            </div>
          </div>

          <div className="text-center">
            <button
              type="submit"
              className="btn btn-primary"
              onClick={this.handleSubmit}
            >
              Submit
            </button>
          </div>
        </form>

        <div className={this.state.hasValidated ? "d-block" : "d-none"}>
          <strong>Account has been Created</strong>
        </div>
      </div>
    );
  }
}

export default Form;
